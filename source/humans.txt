# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Sven Wolfermann -- Head of Frontend -- @maddesigns
    Robert Weber -- Slave of Frontend -- @closingtag

# THANKS

    <name>

# TECHNOLOGY COLOPHON

    HTML5, CSS3
    jQuery, Modernizr
