###
# Compass
###
activate :livereload
# Susy grids in Compass
# First: gem install susy
# require 'susy'

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy (fake) files
# page "/this-page-has-no-template.html", :proxy => "/template-file.html" do
#   @which_fake_page = "Rendering a fake page with a variable"
# end

###
# Helpers
###
# @items = [
#   [1,  2,  3,  4,  5],
#   [6,  7,  8,  9,  10],
#   [11, 12, 13, 14, 15]
# ]
@items = [
  [
  "Apple","iPod Touch 2","iOS 4","320 × 480","320 × 480","3,5”",""],[
  "","iPhone 3GS","iOS 6","320 × 480","320 × 480","3,5”",""],[
  "","iPhone 4","iOS 5 ","640 × 960","320 × 480","3,5”",""],[
  "","iPhone 4","iOS 5","640 × 960","320 × 480","3,5”",""],[
  "","iPhone 4","iOS 6","640 × 960","320 × 480","3,5”",""],[
  "","iPhone 5","iOS 6","640 × 1136","320 × 568","4,0”","privat"],[
  "","iPad 2","iOS 5 ","786 × 1024","786 × 1024","9,7”",""],[
  "","iPad 3","iOS 6","1536 × 2048","786 × 1024","9,7”","privat"],[
  "","iPad Mini","iOS 6","786 × 1024","786 × 1024","7,9”","privat"],[
  "Samsung","Galaxy Ace GT-S5830","Android 2.3","320 × 480","320 × 480","3,5”",""],[
  "","Galaxy Tab-PT-P1010","Android 2.2","600 × 1024","400 × 683","7”",""],[
  "","Galaxy Tab-PT-P3110","Android 4.0.4","600 × 1024","600 × 1024","7”",""],[
  "","Galaxy Tab GT-P7510","Android 4.0.4","1280 × 800","1280 × 800","10.1”",""],[
  "","Nexus S","Firefox OS","480 × 800","320 × 533","4”",""],[
  "","Nexus S","Android 4.1.2","480 × 800","320 × 533","4”",""],[
  "","Galaxy S","Android 2.2.1","480 × 800","320 × 533","4”",""],[
  "","Galaxy S II","Android 4.1.2","480 × 800","320 × 533","4”",""],[
  "","Galaxy S II","Firefox OS","480 × 800","320 × 533","4”",""],[
  "","Galaxy S III","Android 4.0.4","720 × 1280","360 × 640","4,8”",""],[
  "","Galaxy Note","Android 4.0.4","800 × 1280","400 × 640","5,3”",""],[
  "","Omnia 7","Windows Phone 7.5","480 × 800","320 × 480","4”",""],[
  "","Wave 8500","Bada 1.0","480 × 800","240 × 400","3,3”",""],[
  "","Wave 8600","Bada 2.0","480 × 800","320 × 533","4”",""],[
  "HTC","Desire","Android 2.2","480 × 800","320 × 533","3,7”",""],[
  "","Desire HD","Android 2.3.5","480 × 800","320 × 533","4,3”",""],[
  "","Wildfire","Android 2.2","240 × 320","320 × 427","3,2”",""],[
  "","Mozart","Windows Phone 7.5","480 ž 800","320 × 480","3,7”",""],[
  "Blackberry","Bold 9700","Blackberry OS 6","360 × 480","320 × NA","2,44”",""],[
  "","Playbook","Tablet OS 2","600 × 1024","600 × 1024","7”",""],[
  "","Curve 9380","Blackberry OS 7","480 × 360","320 × 406","3,2”",""],[
  "Sony Ericsson","Xperia X10 Mini","Android 2.1","240 × 320","240 × 320","2,6”",""],[
  "","Sony Ericsson Xperia arc","Android 2.3.4","480 × 854","320 × 569","4.2”",""],[
  "Lenovo","IdeaPad A1","Android 2.3","600 × 1024","400 × 638","7”",""],[
  "Arival","BioniQ 700","Android 4.0","480 × 800","","7”",""],[
  "Nokia","C5-03","S60, OS 9.4","360 × 640","360 × 480","3,2”",""],[
  "","N9","MeeGo","480 × 854","???","3,9”",""],[
  "","Asha 311","S60, OS 9.4","","","",""],[
  "Motorola","Xoom","Android 3.2","800 × 1280","800 × 1280","8,2”",""],[
  "","Milestone (Droid)","Android 2.2","480 × 854","320 × 569","3,7”",""],[
  "Asus","Nexus 7","Android 4.2","800 × 1280","600 × 961","7”",""],[
  "","Eee Pad Transformer TF101","Android 4.0.4","800 × 1280","800 × 1280","10”",""],[
  "Amazon","Kindle Fire","7.2.1. (Android Port)","600 × 1024","600 × 1024","7”",""],[
  "","Kindle Fire HD","7.2.1. (Android Port)","800 × 1280","533 × 801","7”",""],[
  "","Kindle Fire HD 8.9","7.2.1. (Android Port)","1200 × 1920","800 × 1220","8,9”",""],[
  "ZTE","Racer II","Android 2.2.2","240 × 320","320 × 427","2,8”",""],[
  "","V9 (Base Tab)","Android 2.2","480 × 800","480 × 800","7”",""],[
  "LG","Nexus 4","Android 4.2.1","768 × 1280 ","384 × 598","",""]
]

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

set :css_dir, 'css'

set :js_dir, 'js'

set :images_dir, 'img'

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  activate :cache_buster

  # Use relative URLs
  #activate :relative_assets

  # Compress PNGs after build
  # First: gem install middleman-smusher
  # require "middleman-smusher"
  # activate :smusher

  # Or use a different image path
  # set :http_path, "/Content/images/"
end
